//
//  TrainCalculateStopsViewController.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import Foundation
import UIKit
import Font_Awesome_Swift
import MTBBarcodeScanner

class TrainCalculateStopsViewController: UIViewController {
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var stationInfoButton: UIButton!
    
    var timeTimer: Timer!
    var trainScheduleTimer: Timer!
    var scanner: MTBBarcodeScanner?
    
    @IBOutlet weak var cameraView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stationNameLabel.text = "Buckhead"
        updateTimeLabel()
        timeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TrainViewController.updateTimeLabel), userInfo: nil, repeats: true)
        
        directionsButton.layer.shadowColor = UIColor(hexStr: "#00546C", alpha: 1.0).cgColor
        directionsButton.layer.shadowOpacity = 1.0
        directionsButton.layer.shadowRadius = 0.0
        directionsButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        stationInfoButton.layer.shadowColor = UIColor(hexStr: "#834C23", alpha: 1.0).cgColor
        stationInfoButton.layer.shadowOpacity = 1.0
        stationInfoButton.layer.shadowRadius = 0.0
        stationInfoButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        let proceedGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TrainCalculateStopsViewController.openScanner))
        proceedGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(proceedGestureRecognizer)
        
        scanner = MTBBarcodeScanner(previewView: cameraView)
    }
    
    // data functions
    func updateTimeLabel() {
        let now: Date = Date()
        let dfmt: DateFormatter = DateFormatter()
        dfmt.dateFormat = "hh:mm a"
        timeLabel.text = dfmt.string(from: now)
    }
    
    func openScanner() {
        MTBBarcodeScanner.requestCameraPermission(success: { (success) in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { (codes) in
                        let codeObjects = codes as! [AVMetadataMachineReadableCodeObject]?
                        
                        for code in codeObjects! {
                            let stringValue = code.stringValue!
                            print("Found code: \(stringValue)")
                            self.scanner?.stopScanning()

                            self.performSegue(withIdentifier: "goToInTrainInfo", sender: nil)
                        }
                    })
                } catch {
                    print("There was an error \(error)")
                }
            } else {
                print("camera not supported")
            }
        })
    }
}
