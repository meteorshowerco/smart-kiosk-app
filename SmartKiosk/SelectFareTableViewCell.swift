//
//  SelectFareTableViewCell.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import UIKit

class SelectFareTableViewCell: UITableViewCell {

    @IBOutlet weak var fareContainerView: UIView!
    @IBOutlet weak var fareCountLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // rounded
        fareContainerView.layer.cornerRadius = fareContainerView.frame.size.width/2.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
