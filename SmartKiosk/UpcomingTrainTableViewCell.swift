//
//  UpcomingTrainTableViewCell.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/24/17.
//  Copyright © 2017 jk. All rights reserved.
//

import UIKit

class UpcomingTrainTableViewCell: UITableViewCell {

    @IBOutlet weak var directionContainerView: UIView!
    @IBOutlet weak var directionLabel: UILabel!
    @IBOutlet weak var waitingTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // rounded
        directionContainerView.layer.cornerRadius = directionContainerView.frame.size.width/2.0
        directionContainerView.layer.borderColor = UIColor.white.cgColor
        directionContainerView.layer.borderWidth = 6.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTrainData(data: RealTimeTrainData) {
        directionLabel.text = data.direction
        waitingTimeLabel.text = data.waitingTime
        
        if data.waitingTime == "Arriving" || data.waitingTime == "Boarding" {
            waitingTimeLabel.font = UIFont(name: "Soleil-Bold", size: 46.0)
        } else {
            waitingTimeLabel.font = UIFont(name: "Soleil", size: 46.0)
        }
    }
    
    func showNoTrainsLeft() {
        directionLabel.text = "X"
        waitingTimeLabel.text = "No trains available"
    }
    
}
