//
//  SelectFareViewController.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import Foundation
import UIKit
import Font_Awesome_Swift
import Alamofire
import SwiftyJSON

class SelectFareViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var northTrainContainer: UIView!
    @IBOutlet weak var northTrainLabel: UILabel!
    @IBOutlet weak var southTrainContainer: UIView!
    @IBOutlet weak var southTrainLabel: UILabel!
    
    @IBOutlet weak var yourCurrentFareLabel: UILabel!
    @IBOutlet weak var fareAmountLabel: UILabel!
    @IBOutlet weak var roundTripCheckerLabel: UILabel!
    
    @IBOutlet weak var fareInstructionsTableView: UITableView!
    let fareReuseId: String = "selectFareCell"
    var trainTimeSource: [RealTimeTrainData] = [RealTimeTrainData]()

    @IBOutlet weak var purchaseFareButton: UIButton!
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var stationInfoButton: UIButton!
    
    @IBOutlet weak var northLabel: UILabel!
    @IBOutlet weak var northConstraint: NSLayoutConstraint!
    @IBOutlet weak var southConstraint: NSLayoutConstraint!
    @IBOutlet weak var southLabel: UILabel!
    
    var timeTimer: Timer!
    var trainScheduleTimer: Timer!
    var isRoundTrip: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fareInstructionsTableView.register(UINib(nibName: "SelectFareTableViewCell", bundle: nil), forCellReuseIdentifier: fareReuseId)
        fareInstructionsTableView.delegate = self
        fareInstructionsTableView.dataSource = self
        
        stationNameLabel.text = "Buckhead"
        updateTimeLabel()
        timeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(SelectFareViewController.updateTimeLabel), userInfo: nil, repeats: true)
        
        northTrainContainer.layer.cornerRadius = 20.5
        northTrainContainer.layer.borderColor = UIColor.white.cgColor
        northTrainContainer.layer.borderWidth = 3.0
        northTrainLabel.layer.cornerRadius = 4.0
        northTrainLabel.layer.borderColor = UIColor(hexStr: "FF001F", alpha: 1.0).cgColor
        northTrainLabel.layer.borderWidth = 1.0
        northTrainLabel.layer.masksToBounds = true
        
        southTrainContainer.layer.cornerRadius = 20.5
        southTrainContainer.layer.borderColor = UIColor.white.cgColor
        southTrainContainer.layer.borderWidth = 3.0
        southTrainLabel.layer.cornerRadius = 4.0
        southTrainLabel.layer.borderColor = UIColor(hexStr: "FF001F", alpha: 1.0).cgColor
        southTrainLabel.layer.borderWidth = 1.0
        southTrainLabel.layer.masksToBounds = true
        
        yourCurrentFareLabel.font = UIFont(name: "SoleilBk", size: 48.0)
        //        temperatureLabel.font = UIFont(name: "Soleil", size: 130.0)
        fareAmountLabel.text = "$5.00"
        roundTripCheckerLabel.setFAText(prefixText: "Round Trip?  ", icon: .FACheckSquareO, postfixText: "", size: 48.0, iconSize: 60.0)
        let roundTripRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SelectFareViewController.toggleRoundTrip))
        roundTripCheckerLabel.isUserInteractionEnabled = true
        roundTripCheckerLabel.addGestureRecognizer(roundTripRecognizer)
        
        fareInstructionsTableView.layer.borderWidth = 3.0
        fareInstructionsTableView.layer.borderColor = UIColor.white.cgColor
        
        // drop shadow
        purchaseFareButton.layer.shadowColor = UIColor(hexStr: "#836220", alpha: 0.99).cgColor
        purchaseFareButton.layer.shadowOpacity = 1.0
        purchaseFareButton.layer.shadowRadius = 0.0
        purchaseFareButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        directionsButton.layer.shadowColor = UIColor(hexStr: "#00546C", alpha: 1.0).cgColor
        directionsButton.layer.shadowOpacity = 1.0
        directionsButton.layer.shadowRadius = 0.0
        directionsButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        stationInfoButton.layer.shadowColor = UIColor(hexStr: "#834C23", alpha: 1.0).cgColor
        stationInfoButton.layer.shadowOpacity = 1.0
        stationInfoButton.layer.shadowRadius = 0.0
        stationInfoButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        let proceedGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SelectFareViewController.goToPurchase))
        proceedGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(proceedGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // poll data every 10s
        getStationTrainData()
        trainScheduleTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(WelcomeViewController.getStationTrainData), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        trainScheduleTimer.invalidate()
    }
    
    // tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: fareReuseId, for: indexPath) as! SelectFareTableViewCell
        
        if indexPath.item == 0 {
            cell.fareCountLabel.text = "A"
            cell.instructionLabel.text = "Add more trips"
        } else if indexPath.item == 1 {
            cell.fareCountLabel.text = "B"
            cell.instructionLabel.text = "Reload to Breezecard"
        } else {
            cell.fareCountLabel.text = "C"
            cell.instructionLabel.text = "Add to new Breezecard"
        }
        
        return cell
    }
    
    // data functions
    func updateTimeLabel() {
        let now: Date = Date()
        let dfmt: DateFormatter = DateFormatter()
        dfmt.dateFormat = "hh:mm a"
        timeLabel.text = dfmt.string(from: now)
    }
    
    func toggleRoundTrip() {
        if isRoundTrip {
            fareAmountLabel.text = "$2.50"
            roundTripCheckerLabel.setFAText(prefixText: "Round Trip?  ", icon: .FASquareO, postfixText: "", size: 48.0, iconSize: 60.0)
            isRoundTrip = false
        } else {
            fareAmountLabel.text = "$5.00"
            roundTripCheckerLabel.setFAText(prefixText: "Round Trip?  ", icon: .FACheckSquareO, postfixText: "", size: 48.0, iconSize: 60.0)
            isRoundTrip = true
        }
    }
    
    func goToPurchase() {
        self.performSegue(withIdentifier: "goToPurchase", sender: self)
    }
    
    func getStationTrainData() {
        // clear data
        trainTimeSource.removeAll()
        
        let parameters: Parameters = ["apikey": "23321c6f-df17-4b4c-8731-1085bca4630d"]
        
        Alamofire.request("http://developer.itsmarta.com/RealtimeTrain/RestServiceNextTrain/GetRealtimeArrivals", method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            
            if let value = response.result.value {
                let json: JSON = JSON(value)
                
                for (_, stationData) : (String, JSON) in json {
                    if stationData["STATION"].string == "BUCKHEAD STATION" {
                        print(stationData)
                        let tempData: RealTimeTrainData = RealTimeTrainData()
                        tempData.direction = stationData["DIRECTION"].string!
                        tempData.waitingTime = stationData["WAITING_TIME"].string!
                        self.trainTimeSource.append(tempData)
                    }
                }
                
                // now based on the data, let's update the slidey things
                var northFound: Bool = false
                var southFound: Bool = false
                for trainData in self.trainTimeSource {
                    
                    if trainData.direction == "N" {
                        if !northFound {
                            if trainData.waitingTime == "Arriving" || trainData.waitingTime == "Boarding" {
                                self.northLabel.text = "Here"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.northConstraint.constant = -290.0
                                    self.view.layoutIfNeeded()
                                })
                            } else {
                                // strip out letters
                                let strippedNum = trainData.waitingTime.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890").inverted)
                                let proportionalLine = 171.0/30.0
                                let gains = Double(strippedNum)!*proportionalLine
                                self.northLabel.text = "Arriving: \(trainData.waitingTime)"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.northConstraint.constant = CGFloat(-1.0*(290+gains))
                                    self.view.layoutIfNeeded()
                                })
                            }
                            
                            northFound = true
                        }
                    } else if trainData.direction == "S" {
                        if !southFound {
                            if trainData.waitingTime == "Arriving" || trainData.waitingTime == "Boarding" {
                                self.southLabel.text = "Here"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.southConstraint.constant = -290.0
                                    self.view.layoutIfNeeded()
                                })
                            } else {
                                // strip out letters
                                let strippedNum = trainData.waitingTime.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890").inverted)
                                let proportionalLine = (169.0)/30.0
                                let gains = (30.0-Double(strippedNum)!)*proportionalLine
                                self.southLabel.text = "Arriving: \(trainData.waitingTime)"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.southConstraint.constant = CGFloat(-1.0*(121.0+gains))
                                    self.view.layoutIfNeeded()
                                })
                            }
                            
                            southFound = true
                        }
                    }
                }
                
                // check if neither found
                if !northFound {
                    self.northLabel.text = "Departed"
                }
                
                if !southFound {
                    self.southLabel.text = "Departed"
                }
            }
        }
    }
}
