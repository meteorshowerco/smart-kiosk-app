//
//  WelcomeViewController.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/24/17.
//  Copyright © 2017 jk. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import Font_Awesome_Swift

class WelcomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var northTrainContainer: UIView!
    @IBOutlet weak var northTrainLabel: UILabel!
    @IBOutlet weak var southTrainContainer: UIView!
    @IBOutlet weak var southTrainLabel: UILabel!
    
    @IBOutlet weak var tapToStartLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var upcomingTrainsTableView: UITableView!
    let upcomingTrainCellReuseId: String = "upcomingTrainCell"
    var trainTimeSource: [RealTimeTrainData] = [RealTimeTrainData]()
    
    @IBOutlet weak var purchaseFareButton: UIButton!
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var stationInfoButton: UIButton!
    
    @IBOutlet weak var languagesButton: UIButton!
    @IBOutlet weak var emergencyButton: UIButton!
    @IBOutlet weak var trashButton: UIButton!
    
    var timeTimer: Timer!
    var trainScheduleTimer: Timer!
    
    @IBOutlet weak var northConstraint: NSLayoutConstraint!
    @IBOutlet weak var northLabel: UILabel!
    @IBOutlet weak var southConstraint: NSLayoutConstraint!
    @IBOutlet weak var southLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upcomingTrainsTableView.register(UINib(nibName: "UpcomingTrainTableViewCell", bundle: nil), forCellReuseIdentifier: upcomingTrainCellReuseId)
        upcomingTrainsTableView.delegate = self
        upcomingTrainsTableView.dataSource = self
        
        stationNameLabel.text = "Buckhead"
        updateTimeLabel()
        timeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(WelcomeViewController.updateTimeLabel), userInfo: nil, repeats: true)
        
        northTrainContainer.layer.cornerRadius = 20.5
        northTrainContainer.layer.borderColor = UIColor.white.cgColor
        northTrainContainer.layer.borderWidth = 3.0
        northTrainLabel.layer.cornerRadius = 4.0
        northTrainLabel.layer.borderColor = UIColor(hexStr: "FF001F", alpha: 1.0).cgColor
        northTrainLabel.layer.borderWidth = 1.0
        northTrainLabel.layer.masksToBounds = true

        southTrainContainer.layer.cornerRadius = 20.5
        southTrainContainer.layer.borderColor = UIColor.white.cgColor
        southTrainContainer.layer.borderWidth = 3.0
        southTrainLabel.layer.cornerRadius = 4.0
        southTrainLabel.layer.borderColor = UIColor(hexStr: "FF001F", alpha: 1.0).cgColor
        southTrainLabel.layer.borderWidth = 1.0
        southTrainLabel.layer.masksToBounds = true
        
        tapToStartLabel.font = UIFont(name: "SoleilBk", size: 48.0)
//        temperatureLabel.font = UIFont(name: "Soleil", size: 130.0)
        temperatureLabel.text = "72°"
        weatherIcon.setFAIconWithName(icon: .FACloud, textColor: .white, backgroundColor: .clear, size: weatherIcon.frame.size)
        
        // drop shadow
        purchaseFareButton.layer.shadowColor = UIColor(hexStr: "#836220", alpha: 0.99).cgColor
        purchaseFareButton.layer.shadowOpacity = 1.0
        purchaseFareButton.layer.shadowRadius = 0.0
        purchaseFareButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        directionsButton.layer.shadowColor = UIColor(hexStr: "#00546C", alpha: 1.0).cgColor
        directionsButton.layer.shadowOpacity = 1.0
        directionsButton.layer.shadowRadius = 0.0
        directionsButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        stationInfoButton.layer.shadowColor = UIColor(hexStr: "#834C23", alpha: 1.0).cgColor
        stationInfoButton.layer.shadowOpacity = 1.0
        stationInfoButton.layer.shadowRadius = 0.0
        stationInfoButton.layer.shadowOffset = CGSize(width: 0, height: 5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // poll data every 10s
        getStationTrainData()
        trainScheduleTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(WelcomeViewController.getStationTrainData), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        trainScheduleTimer.invalidate()
    }
    
    // tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trainTimeSource.count > 0 {
            return trainTimeSource.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: upcomingTrainCellReuseId, for: indexPath) as! UpcomingTrainTableViewCell
        
        if trainTimeSource.count > 0 {
            cell.setTrainData(data: trainTimeSource[indexPath.item])
        } else {
            cell.showNoTrainsLeft()
        }

        return cell
    }
    
    // data functions
    func updateTimeLabel() {
        let now: Date = Date()
        let dfmt: DateFormatter = DateFormatter()
        dfmt.dateFormat = "hh:mm a"
        timeLabel.text = dfmt.string(from: now)
    }
    
    func getStationTrainData() {
        // clear data
        trainTimeSource.removeAll()
        
        let parameters: Parameters = ["apikey": "23321c6f-df17-4b4c-8731-1085bca4630d"]
        
        Alamofire.request("http://developer.itsmarta.com/RealtimeTrain/RestServiceNextTrain/GetRealtimeArrivals", method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            
            if let value = response.result.value {
                let json: JSON = JSON(value)
                
                for (_, stationData) : (String, JSON) in json {
                    if stationData["STATION"].string == "BUCKHEAD STATION" {
                        print(stationData)
                        let tempData: RealTimeTrainData = RealTimeTrainData()
                        tempData.direction = stationData["DIRECTION"].string!
                        tempData.waitingTime = stationData["WAITING_TIME"].string!
                        self.trainTimeSource.append(tempData)
                    }
                }
                
                self.upcomingTrainsTableView.reloadData()
                
                // now based on the data, let's update the slidey things
                var northFound: Bool = false
                var southFound: Bool = false
                for trainData in self.trainTimeSource {
                    
                    if trainData.direction == "N" {
                        if !northFound {
                            if trainData.waitingTime == "Arriving" || trainData.waitingTime == "Boarding" {
                                self.northLabel.text = "Here"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.northConstraint.constant = -290.0
                                    self.view.layoutIfNeeded()
                                })
                            } else {
                                // strip out letters
                                let strippedNum = trainData.waitingTime.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890").inverted)
                                let proportionalLine = 171.0/30.0
                                let gains = Double(strippedNum)!*proportionalLine
                                self.northLabel.text = "Arriving: \(trainData.waitingTime)"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.northConstraint.constant = CGFloat(-1.0*(290+gains))
                                    self.view.layoutIfNeeded()
                                })
                            }
                            
                            northFound = true
                        }
                    } else if trainData.direction == "S" {
                        if !southFound {
                            if trainData.waitingTime == "Arriving" || trainData.waitingTime == "Boarding" {
                                self.southLabel.text = "Here"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.southConstraint.constant = -290.0
                                    self.view.layoutIfNeeded()
                                })
                            } else {
                                // strip out letters
                                let strippedNum = trainData.waitingTime.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890").inverted)
                                let proportionalLine = (169.0)/30.0
                                let gains = (30.0-Double(strippedNum)!)*proportionalLine
                                self.southLabel.text = "Arriving: \(trainData.waitingTime)"
                                UIView.animate(withDuration: 0.1, animations: {
                                    self.southConstraint.constant = CGFloat(-1.0*(121.0+gains))
                                    self.view.layoutIfNeeded()
                                })
                            }
                            
                            southFound = true
                        }
                    }
                }
                
                // check if neither found
                if !northFound {
                    self.northLabel.text = "Departed"
                }
                
                if !southFound {
                    self.southLabel.text = "Departed"
                }
            }
        }
    }
}
