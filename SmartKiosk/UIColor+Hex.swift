//
//  UIColor+Hex.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hexStr: String, alpha: CGFloat) {
        let hex = hexStr.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        
        assert(hex.characters.count == 6, "Invalid hex string supplied")
        
        let red, green, blue: UInt32
        (red, green, blue) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
}
