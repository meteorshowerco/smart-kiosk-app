//
//  PurchaseFareTableViewCell.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import UIKit

class PurchaseFareTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var instructionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
