//
//  TrainViewController.swift
//  SmartKiosk
//
//  Created by Jeffery Kuo on 2/25/17.
//  Copyright © 2017 jk. All rights reserved.
//

import Foundation
import UIKit
import Font_Awesome_Swift
import MTBBarcodeScanner

class TrainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var tapToStartLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var upcomingTrainsTableView: UITableView!
    let upcomingTrainCellReuseId: String = "upcomingTrainCell"
    
    @IBOutlet weak var purchaseFareButton: UIButton!
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var stationInfoButton: UIButton!
    
    @IBOutlet weak var languagesButton: UIButton!
    @IBOutlet weak var emergencyButton: UIButton!
    
    var timeTimer: Timer!
    var trainScheduleTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        upcomingTrainsTableView.register(UINib(nibName: "UpcomingTrainTableViewCell", bundle: nil), forCellReuseIdentifier: upcomingTrainCellReuseId)
        upcomingTrainsTableView.delegate = self
        upcomingTrainsTableView.dataSource = self
        
        stationNameLabel.text = "Red Train"
        updateTimeLabel()
        timeTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TrainViewController.updateTimeLabel), userInfo: nil, repeats: true)
        
        tapToStartLabel.font = UIFont(name: "SoleilBk", size: 48.0)
        //        temperatureLabel.font = UIFont(name: "Soleil", size: 130.0)
        temperatureLabel.text = "72°"
        weatherIcon.setFAIconWithName(icon: .FACloud, textColor: .white, backgroundColor: .clear, size: weatherIcon.frame.size)
        
        // drop shadow
        purchaseFareButton.layer.shadowColor = UIColor(hexStr: "#836220", alpha: 0.99).cgColor
        purchaseFareButton.layer.shadowOpacity = 1.0
        purchaseFareButton.layer.shadowRadius = 0.0
        purchaseFareButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        directionsButton.layer.shadowColor = UIColor(hexStr: "#00546C", alpha: 1.0).cgColor
        directionsButton.layer.shadowOpacity = 1.0
        directionsButton.layer.shadowRadius = 0.0
        directionsButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        stationInfoButton.layer.shadowColor = UIColor(hexStr: "#834C23", alpha: 1.0).cgColor
        stationInfoButton.layer.shadowOpacity = 1.0
        stationInfoButton.layer.shadowRadius = 0.0
        stationInfoButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        let proceedGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TrainViewController.proceed))
        proceedGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(proceedGestureRecognizer)
    }
    
    // tableview delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: upcomingTrainCellReuseId, for: indexPath) as! UpcomingTrainTableViewCell
        
        if indexPath.item == 0 {
            cell.directionLabel.text = "2"
            cell.waitingTimeLabel.text = "Lindbergh"
            cell.waitingTimeLabel.font = UIFont(name: "Soleil-Bold", size: 36.0)
        } else if indexPath.item == 1 {
            cell.directionLabel.text = "3"
            cell.waitingTimeLabel.text = "Arts Center"
        } else {
            cell.directionLabel.text = "4"
            cell.waitingTimeLabel.text = "Midtown"
        }
        
        return cell
    }
    
    // data functions
    func updateTimeLabel() {
        let now: Date = Date()
        let dfmt: DateFormatter = DateFormatter()
        dfmt.dateFormat = "hh:mm a"
        timeLabel.text = dfmt.string(from: now)
    }
    
    func proceed() {
        self.performSegue(withIdentifier: "goToBreezeScan", sender: nil)
    }
    
}
